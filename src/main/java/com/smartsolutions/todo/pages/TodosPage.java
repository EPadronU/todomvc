package com.smartsolutions.todo.pages;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;
import static org.openqa.selenium.support.locators.RelativeLocator.with;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfAllElementsLocatedBy;

import java.util.List;

import com.smartsolutions.core.Page;
import com.smartsolutions.todo.components.TodoItemComponent;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public final class TodosPage extends Page {

  private static final By selectAllLabel = By.cssSelector("label[for='toggle-all]");

  private static final By newTodoInbox = By.className("new-todo");

  private static final By todoItems = By.className("todo");

  private static final By clearCompletedButton = By.className("clear-completed");

  private static final By todoCountSpan = By.className("todo-count");

  public enum Filter {
    ALL(By.linkText("All")),
    ACTIVE(By.linkText("Active")),
    COMPLETED(By.linkText("Completed"));
    /*ALL(with(By.linkText("All")).toRightOf(todoCountSpan)),
     ACTIVE(with(By.linkText("Active")).near(ALL.selector)),
     COMPLETED(with(By.linkText("Completed")).toLeftOf(clearCompletedButton));*/

    private final By selector;

    private Filter(final By selector) {
      this.selector = selector;
    }
  }

  public TodosPage(final WebDriver webDriver) {
    super(webDriver);
  }

  public TodosPage toggleSelectAll() {
    waitFor(elementToBeClickable(selectAllLabel)).click();

    return this;
  }

  public TodosPage createNewTodo(final String whatNeedsToBeDone) {
    waitFor(presenceOfElementLocated(newTodoInbox)).sendKeys(whatNeedsToBeDone, Keys.ENTER);

    return this;
  }

  public List<TodoItemComponent<TodosPage>> getTodoItems() {
    return hasMany(TodoItemComponent::new, waitFor(presenceOfAllElementsLocatedBy(todoItems)));
  }

  public TodosPage filterBy(final Filter filter) {
    waitFor(elementToBeClickable(filter.selector)).click();

    return this;
  }

  public TodosPage clearCompleted() {
    waitFor(elementToBeClickable(clearCompletedButton)).click();

    return this;
  }
}
