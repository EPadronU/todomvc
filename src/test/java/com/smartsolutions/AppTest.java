/* ************************************************************************************************/
package com.smartsolutions;
/* ************************************************************************************************/

/* ************************************************************************************************/
import static org.assertj.core.api.Assertions.assertThat;

import java.time.Duration;

import com.smartsolutions.core.Browser;
import com.smartsolutions.todo.components.TodoItemComponent;
import com.smartsolutions.todo.pages.TodosPage;
import com.smartsolutions.todo.pages.TodosPage.Filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.EntryMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.openqa.selenium.firefox.FirefoxDriver;
/* ************************************************************************************************/

/* ************************************************************************************************/
public class AppTest {

  private static final Logger log = LogManager.getLogger();

  @Test
  void newTodoItemCreation(final TestInfo testInfo) {
    final EntryMessage entryMessage = log.traceEntry("Running test {}", testInfo.getDisplayName());

    Browser.navigate(browser -> {
      // Given I navigate to the todos page
      final TodosPage page = browser.goTo(TodosPage::new, "https://todomvc.com/examples/vue/");

      // When I create a new todo
      page.createNewTodo("Prepare the session for the guys and gals");

      // Then there should be only one todo item with the expected text
      assertThat(page.getTodoItems())
          .describedAs("Only one todo item must exists and it should have the expected text")
          .hasSize(1)
          .first()
          .extracting(TodoItemComponent::getContent)
          .isEqualTo("Prepare the session for the guys and gals");

      pause();
    });

    log.traceExit(entryMessage);
  }

  @Test
  void newTodoItemCreationAndFilterByActive(final TestInfo testInfo) {
    final EntryMessage entryMessage = log.traceEntry("Running test {}", testInfo.getDisplayName());

    Browser.navigate(FirefoxDriver::new, browser -> {
      // Given I navigate to the todos page
      final TodosPage page = browser.goTo(TodosPage::new, "https://todomvc.com/examples/vue/");

      // When I create a new todo and filter by Active
      page.createNewTodo("Prepare the session for the guys and gals")
          .filterBy(Filter.ACTIVE);

      // Then there should be only one todo item with the expected text
      assertThat(page.getTodoItems())
          .describedAs("Only one todo item must exists and it should have the expected text")
          .hasSize(1)
          .first()
          .extracting(TodoItemComponent::getContent)
          .isEqualTo("Prepare the session for the guys and gals");

      pause();
    });

    log.traceExit(entryMessage);
  }

  @Test
  void complexInteraction(final TestInfo testInfo) {
    final EntryMessage entryMessage = log.traceEntry("Running test {}", testInfo.getDisplayName());

    Browser.navigate(browser -> {
      // Given I navigate to the todos page
      final TodosPage page = browser.goTo(TodosPage::new, "https://todomvc.com/examples/vue/");

      // When I get creative
      page.createNewTodo("Active todo")
          .createNewTodo("Completed todo")
          .getTodoItems()
          .get(1)
          .toggle()
          .getPage()
          .filterBy(Filter.COMPLETED)
          .getTodoItems()
          .get(0)
          .destroy()
          .filterBy(Filter.ACTIVE);

      // Then there should be only one todo item with the expected text
      assertThat(page.getTodoItems())
          .describedAs("Only one todo item must exists and it should have the expected text")
          .hasSize(1)
          .first()
          .extracting(TodoItemComponent::getContent)
          .isEqualTo("Active todo");

      pause();
    });

    log.traceExit(entryMessage);
  }

  private static void pause() {
    try {
      Thread.sleep(Duration.ofSeconds(5L));
    } catch (final InterruptedException e) {
      e.printStackTrace();
    }
  }
}
/* ************************************************************************************************/
